Standalone Solr Helm Chart
--------------------------

This chart provides a general-purpose standalone Solr instance with persistence.

If you need to create a core "mycore" on startup, set 'commandArgs' to ["solr-precreate", "mycore"].
More complex startup processes can use a ConfigMap of shell scripts if the name is provided in 'initdbConfig'.
Or, you can provide your own volumes and volumeMounts configs to add to the container.

The chart by default uses the official Solr images hosted on DockerHub. You can
specify an alternate repository by setting 'image.repository'.  It is recommended
to specify a tag other than the default 'latest'.

A persistent data store mounted by default at /var/solr in the container.  You can
use an existing PVC by setting 'persistence.existingClaim'; otherwise, a default PVC
will be provided.  You can customize the mountPath if necessary.
